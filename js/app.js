console.log('greenSpots');

/* Screen */
   const screenHeight = $(window).height();
   const screenWidth = $(window).width();

   const docGetElemMapId = $('#mapid');
   docGetElemMapId.css({'height': `${screenHeight}px`, 'width': `${screenWidth}px`});

/* Drawer */
   const drawer = $('#drawer');

   function openNav() {
      drawer.css('width', '250px');
   }

   function closeNav() {
      drawer.css('width', '0');
   }

/* Map */
   const gsMap = L.map('mapid').setView([48.210033, 16.373000], 12);
   const aToken = 'pk.eyJ1IjoiY2FsdWFwcHMiLCJhIjoiY2pnYjVvZm93MXc3dDJ4cWpvZGg5aGFzMyJ9.d6XugY48o8qj8oBVmS-_yQ';
   const tiles = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}';

   var gsMapLayers = [];
   L.tileLayer(tiles, {
         attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
         maxZoom: 18,
         id: 'mapbox.streets',
         // accessToken: 'pk.eyJ1IjoiY2FsdWFwcHMiLCJhIjoiY2pnYjVvZm93MXc3dDJ4cWpvZGg5aGFzMyJ9.d6XugY48o8qj8oBVmS-_yQ'
         accessToken: aToken
      }).addTo(gsMap);

   /* Icons */
   let userIcon = L.icon({
      iconUrl: 'src/user-red.png',
      iconSize: [64, 64],
      iconAnchor: [32, 64],
      popupAnchor: [],
      shadowUrl: '',
      shadowSize: [],
      shadowAnchor: []
   });

   let playgroundIcon = L.icon({
      iconUrl: 'src/greenspots.png',
      iconSize: [64, 64],
      iconAnchor: [32, 64],
      popupAnchor: [],
      shadowUrl: '',
      shadowSize: [],
      shadowAnchor: []
   });

/* Functions */

   function getLocation() {
      console.log('getLocation()');

      navigator.geolocation.getCurrentPosition((position) => {
         /* outputs calculated position
            console.log(position);
            console.log('latitude:', position.coords.latitude);
            console.log('longitude:', position.coords.longitude); */

         /* mark user position */
         L
            .marker([
                  position.coords.latitude,
                  position.coords.longitude
               ],
               { icon: userIcon })
            .addTo(gsMap);

         /* pan to to area of location */
         gsMap.flyTo([position.coords.latitude, position.coords.longitude], 17);

      }, (err) => {
         alert('Unable to fetch location');

      }, {
         maximumAge: 60000,
         timeout: 10000,
         enableHighAccuracy: false
      });
   }

   function showPosition(position) {
      /* outputs calculated position
         console.log('showPosition(): ', position);
         console.log('Latitude:', position.coords.latitude);
         console.log('Longitude', position.coords.longitude); */

      L.marker([
                  position.coords.latitude,
                  position.coords.longitude
               ]
               .addTo(gsMap));
   }

   function getPlaygrounds() {
      console.log('getPlaygrounds()');
      const playgroundUrl = 'src/SPIELPLATZOGD.json';
      /* outputs true or false if passed coords are inbounds
         console.log(gsMap
                        .getBounds()
                        .contains([48.239432739883384, 16.300753052068533]));
      
         console.log(gsMap.getBounds().contains([48.22969662557311, 16.37140821325929])); */

      /* fetches data from either JSON local File or OpenData API */
      fetch(playgroundUrl)
         .then((res) => res.json())
         .then(function(data) {
            /* outputs responded data
               console.log(data); */
            const allPlaygroundsInVienna = data.features;
            let playgroundArray = [];
            console.log(allPlaygroundsInVienna);
            console.log(allPlaygroundsInVienna[1].geometry.coordinates[0]);
            console.log(allPlaygroundsInVienna[1].geometry.coordinates[1]);

            /* filters playground which are in bounds of visible map */
            playgroundArray = allPlaygroundsInVienna
                                       .filter(plygrnd => gsMap
                                                            .getBounds()
                                                            .contains([
                                                               plygrnd.geometry.coordinates[1],
                                                               plygrnd.geometry.coordinates[0]]));

            /* outputs all filtered
            console.log(playgroundArray); */

            /* shows each filtered playground inbounds */
            playgroundArray.forEach(function(plygrnd) {

               /* checks wich one latitude and longitude is
                  console.log(`
                           latitude: ${plygrnd.geometry.coordinates[1]},
                           longitude: ${plygrnd.geometry.coordinates[0]}`); */
               L
                  .marker([
                     plygrnd.geometry.coordinates[1],
                     plygrnd.geometry.coordinates[0]
                     ],
                     { icon: playgroundIcon })
                  .addTo(gsMap);
            });

         })
         .catch(function() {
            console.log('There has been an Error!');
         });
   }

$('document').ready(function() {

});